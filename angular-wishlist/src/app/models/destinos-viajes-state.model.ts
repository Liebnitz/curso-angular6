import { Injectable } from '@angular/core';
import { Effect, ofType,Actions} from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { DestinoViaje } from './destino-viaje.model';
import {map} from 'rxjs/operators'
import{HttpClient, HttpClientModule, HttpHeaders, HttpRequest, HttpResponse} from '@angular/common/http';


//Estado
export interface DestinosViajesState{
    items: DestinoViaje[];
    loading:boolean;
    favorito:DestinoViaje;
}

export const intializeDestinosViajesState = function(){
    return{
        items:[],
        loading:false,
        favorito:null,
    
    }
}

//Acciones

export enum DestinosViajesActionsTypes{
    NUEVO_DESTINO='[Destinos Viaje] nuevo',
    ELEGIDO_FAVORITO='[Destinos Viajes] favorito',
    VOTE_UP='[Destinos Viajes] Vote Up',
    VOTE_DOWN='[Destinos Viajes] Vote Down',
    INIT_MY_DATA ='[Destinos Viajes] Init My Data'
}


export class InitMyDataAction implements Action{
    type= DestinosViajesActionsTypes.INIT_MY_DATA;
    constructor(public destinos:string[]){}
}

export class NuevoDestinoAction implements Action{
    type = DestinosViajesActionsTypes.NUEVO_DESTINO;
    constructor(public destino:DestinoViaje){}
}


export class ElegidoFavoritoAction implements Action{
    type = DestinosViajesActionsTypes.ELEGIDO_FAVORITO;
    constructor(public destino:DestinoViaje){}
}


export class VoteUpAction implements Action{
    type = DestinosViajesActionsTypes.VOTE_UP;
    constructor(public destino:DestinoViaje){}
}

export class VoteDownAction implements Action{
    type = DestinosViajesActionsTypes.VOTE_DOWN;
    constructor(public destino:DestinoViaje){}
}

export type DestinosViajesActions = NuevoDestinoAction | ElegidoFavoritoAction | VoteUpAction | VoteDownAction | InitMyDataAction;

//Reducers

export function reducerDestinosViajes (
    state:DestinosViajesState,
    action:DestinosViajesActions): DestinosViajesState{
     switch(action.type){
         case DestinosViajesActionsTypes.INIT_MY_DATA:{
             const destinos:string[]=(action as InitMyDataAction).destinos;
             return{
                ...state,
                items:destinos.map((d) => new DestinoViaje(d,''))
             };
         }
         case DestinosViajesActionsTypes.NUEVO_DESTINO:{
         return{
             ...state,
             items:[...state.items,(action as NuevoDestinoAction).destino]
         };
         }
         case DestinosViajesActionsTypes.ELEGIDO_FAVORITO:{
             state.items.forEach(x => x.setSelected(false));
            const fav:DestinoViaje=(action as ElegidoFavoritoAction).destino;
            fav.setSelected(true);
            return{
                ...state,
                favorito: fav
            };
         }
         case DestinosViajesActionsTypes.VOTE_UP:{
           const d:DestinoViaje=(action as VoteUpAction).destino;
           d.voteUp();
           return{ ...state};
           
        }

        case DestinosViajesActionsTypes.VOTE_DOWN:{
           const d:DestinoViaje=(action as VoteDownAction).destino;
           d.voteDown();
           return{ ...state};
           
        }
     }
     return state;
    }

    //Effects

    @Injectable()
    export class DestinosViajesEffects{
        @Effect()
        nuevoAgregado$: Observable<Action>=this.actions$.pipe(
            ofType(DestinosViajesActionsTypes.NUEVO_DESTINO),
            map((action: NuevoDestinoAction)=> new ElegidoFavoritoAction(action.destino))
        );

        constructor(private actions$: Actions){}

    }

    