import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AppState } from '../../app.module';
import { DestinoApiClient } from '../../models/destino-api-client.model';
import { DestinoViaje} from '../../models/destino-viaje.model';
import {Store} from '@ngrx/store';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viajes-state.model';
//import { threadId } from 'worker_threads';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers:[DestinoApiClient]
})
export class ListaDestinosComponent implements OnInit {
//destinos:DestinoViaje[];
@Output() onItemAdded:EventEmitter<DestinoViaje>;
updates:String[];
all;
  constructor( private destinoApiClient:DestinoApiClient, private store:Store<AppState> ) {
    this.onItemAdded = new EventEmitter();
// this.destinos=['Panamá','Los Santos','Veraguas','Coclé']
  // this.destinos=[];
   this.updates=[];
   this.store.select(state => state.destinos.favorito).subscribe(d => {
    if(d !=null){
      this.updates.push('Se ha elegido '+ d.nombre);
    }
  });
  store.select(state=>state.destinos.items).subscribe(items => this.all=items);
  }

  ngOnInit(): void {
  }

   agregado(d:DestinoViaje){
   this.destinoApiClient.add(d);
   this.onItemAdded.emit(d);
  // this.store.dispatch(new NuevoDestinoAction(d) );
   }

   elegido(e:DestinoViaje){
 this.destinoApiClient.elegir(e);
 //this.store.dispatch(new ElegidoFavoritoAction(e) );
   }

   getAll(){

   }

}
