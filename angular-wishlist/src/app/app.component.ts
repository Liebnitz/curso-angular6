import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { DestinoViaje } from './models/destino-viaje.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  destino:DestinoViaje;
  title = 'angular-wishlist';
time= new Observable(observer => {
setInterval(() => observer.next(new Date().toString()),1000);
});

constructor(public translate:TranslateService){
  console.log('**************** get traslation');
  translate.getTranslation('en').subscribe(x => console.log('x:' + JSON.stringify(x)));
  translate.setDefaultLang('es');
}

  destinoAgregado(d){

  }
}
